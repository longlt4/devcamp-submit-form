import 'bootstrap'
import { Component } from "react";

class FormComponent extends Component {
    inputFirstNameHandle(param) {
        console.log(param.target.value)
    };

    inputLastNameHandle(param) {
        console.log(param.target.value)
    };

    inputSelectHandle(param) {
        console.log(param.target.value)
    };

    inputAreaHandle(param) {
        console.log(param.target.value)
    };

    onBtnClickHandle() {
        console.log("Nút Click được bấm")
    };

    render() {
        return (
                <div className="container jumbotron">
                        <div className="mb-3 row">
                            <label className="col-sm-2 col-form-label">First Name</label>
                            <div className="col-sm-10">
                                <input className="form-control" placeholder="Your name.." onChange={this.inputFirstNameHandle}></input>
                            </div>
                        </div>
                        <div className="mb-3 row">
                            <label className="col-sm-2 col-form-label">Last Name</label>
                            <div className="col-sm-10">
                                <input className="form-control" placeholder="Your last name.." onChange={this.inputLastNameHandle}></input>
                            </div>
                        </div>
                        <div className="mb-3 row">
                            <label className="col-sm-2 col-form-label">Country</label>
                            <div className="col-sm-10">
                                <select className="form-select" aria-label="Default select example" onChange={this.inputSelectHandle}>
                                    <option defaultValue>Open this select menu</option>
                                    <option defaultValue="1">One</option>
                                    <option defaultValue="2">Two</option>
                                    <option defaultValue="3">Three</option>
                                </select>
                            </div>
                        </div>
                        <div className="mb-3 row">
                            <label className="col-sm-2 col-form-label">Subject</label>
                            <div className="col-sm-10">
                                <textarea className="form-control" placeholder="Write something" onChange={this.inputAreaHandle}></textarea>
                            </div>
                        </div>
                    <div>
                        <button className="btn btn-success" onClick={this.onBtnClickHandle}>Send Data</button>
                    </div>
                </div>
        )
    }
}
export default FormComponent;