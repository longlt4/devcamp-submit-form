import 'bootstrap/dist/css/bootstrap.min.css'
import TitleComponent from './components/TitleComponent';
import FormComponent from './components/FormComponent';

function App() {
  return (
    <div className="container">
     <TitleComponent/>
     <FormComponent/>
    </div>
  );
}

export default App;
